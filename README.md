#  Solve interview problems

# Ejercicios

Son dos ejercicios para realizar:

1) Consiste en implementar un arreglo con números de los cuáles se van a ordenar de dos en dos con una línea al principio.

2) Se trata de tener un arreglo de correos électronicos de los cuáles necesitamos encontrar todos los terminado en @hotmail.com.


# Run and Building

If you just want to run, you can use a command which it is:

```
node index.js
```


# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


# Authors
* **Jean Pierre Giovanni Arenas Ortiz**

# License
[MIT](https://choosealicense.com/licenses/mit/)