const arreglo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const recorrerArray = (arr) => {
    for (let i = 1; i < arr.length; i+=2) {
        console.log(`- ${arr[i]}`);
    }
}

recorrerArray(arreglo);

const correos = ['primer@hotmail.com', 'segund@hotmail.com', 'tercer@gmail.com', 'cuarto@gmail.com', 'quinto@hotmail.com'];
let correosHotmail = []

// First way for solve this problem
// console.log(correos.pop(correos.includes('tercer@gmail.com')));

// Second way for solution this problem

// let hotmail = correos.filter((correo) => {
//     return correo !== 'tercer@gmail.com';
// })

// console.log(hotmail);

// Third way the best option because its performance

correos.forEach(correo => {
    for (key in correo) {
        if (correo[key] === "@") {
            if (correo.slice(key) === "@hotmail.com") {
                correosHotmail.push(correo)
            }
        }
    }
});

console.log(correosHotmail)